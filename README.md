# Qparser

Este parser foi feito por Breno Moura exclusivamente para o Eloquent Studio.

As informações geradas são separadas na seguinte ordem:

1. Nome do Jogo;
2. Total de mortes;
3. Jogadores da partida;
4. Mortes ocorridas no jogo, já na ordem de ranking. O jogador com mais kills fica em primeiro e vai decrescendo nessa ordem;
5. Rankings de mortes ordenados do maior para o menor, contendo o tipo da morte e a quantidade de vezes que algum player morreu.

## Instalação

Primeiro clone o projeto na sua máquina:

    git clone git@bitbucket.org:basmoura/qparser.git

Depois de clonado, entre no diretório criado:

    cd qparser

Já dentro do diretório execute o seguinte comando para gerar o build do parser:

    gem build qparser.gemspec

Para instalar a gem use o comando:

    gem install qparser-0.0.1.gem

Depois de instalado basta rodar o comando:

    qparser
