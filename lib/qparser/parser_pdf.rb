require './lib/qparser/parser'
require 'prawn'

module Qparser
  class ParserPdf
    def self.generate(games)
      Prawn::Document.generate("QuakeLogParser.pdf", page_size: "A4") do
        text "Quake Log Parser", size: 18, style: :bold, align: :center

        games.each do |k, v|
          text "#{k}".capitalize.gsub!("_", " "), size: 16, style: :bold
          v.each do |key, value|
            if key == :total_kills
              text "#{key}:".capitalize.gsub!("_", " ") + " #{value}"
            end
            move_down 3

            if key == :players
              text "Players", style: :bold
              text value.join(", ")
            end
            move_down 3

            if key == :kills
              text "#{key}".capitalize, style: :bold
              kills = Hash[value.sort_by { |player, kills| kills }.reverse]
              kills.each do |player, kills|
                text "#{player}: #{kills}"
              end
            end

            if key == :kills_by_means
              text "#{key}".capitalize.gsub!("_", " "), style: :bold
              kills = Hash[value.sort_by { |mean, kills| kills }.reverse]
              kills.each do |mean, kills|
                text "#{mean}: #{kills}"
              end
            end
          end

          move_down 10
          stroke_horizontal_rule
          move_down 10
        end
      end
      detect_os
    end

    def self.detect_os
      @os ||= (
        host_os = RbConfig::CONFIG['host_os']
        case host_os
        when /darwin|mac os/
          system('open QuakeLogParser.pdf')
        when /linux/
          system('xdg-open QuakeLogParser.pdf')
        else
          raise Error::WebDriverError, "unknown os: #{host_os.inspect}"
        end
      )
    end
  end
end
