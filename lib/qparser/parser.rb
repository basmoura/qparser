require './lib/qparser/parser_pdf'

module Qparser
  class Parser
    @total_kills = 0
    @players = []
    @games = {}
    @kills = {}
    @kills_by_means = {}
    @kill_regex = /(Kill):.*:.([<A-Za-z> ]*).(killed).([A-Za-z ]*).(by).(MOD[A-Z_]*)$/

    def self.parse(file)
      cont = 0
      file.each do |line|
        get_players(line)
        get_total_kills(line)

        if line.match @kill_regex
          get_kills_per_player($2, $4)
          get_kills_by_means($6)
        end

        if line.match /ShutdownGame:/i
          cont += 1
          @games.merge!( {
            "game_#{cont}" => {
              total_kills: @total_kills,
              players: @players,
              kills: @kills,
              kills_by_means: @kills_by_means
            }
          })
          reset
        end
      end
      ParserPdf.generate(@games)
    end

    def self.get_players(line)
      if line.match /n\\(.*)\\t\\/
        @players << $1 unless @players.include?($1)
      end
    end

    def self.get_total_kills(line)
      @total_kills += 1 if line.match /Kill:/i
    end

    def self.get_kills_per_player(killer, killed)
      @kills[killer] = 0 if !@kills.has_key?(killer) && killer != '<world>'

      if killer != '<world>'
        @kills[killer] += 1
      else
        @kills[killed] = 0 if !@kills.has_key?(killed)
        @kills[killed] -= 1
      end
    end

    def self.get_kills_by_means(mean)
      if @kills_by_means.has_key?(mean)
        @kills_by_means[mean] += 1
      else
        @kills_by_means[mean] = 1
      end
    end

    def self.reset
      @players = []
      @total_kills = 0
      @kills = {}
      @kills_by_means = {}
    end
  end
end
