require './lib/qparser/version'
require './lib/qparser/parser'

module Qparser
  def self.load
    file = File.open('./log/games.log')
    Parser.parse(file)
  end
end
