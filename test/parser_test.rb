require 'minitest/autorun'
require './lib/qparser/parser'

class ParserTest < Minitest::Test
  def setup
    @kill_regex = /(Kill):.*:.([<A-Za-z> ]*).(killed).([A-Za-z ]*).(by).(MOD[A-Z_]*)$/
    @players = []
    @kills_by_means = {}
    @file = File.open('./log/games.log')
  end

  def test_load_file
    assert true if !@file.nil?
  end

  def test_invalid_file_path
    assert_raises Errno::ENOENT do
      File.open('./log/game.log')
    end
  end

  def test_get_players
    @file.each do |line|
      if line.match /n\\(.*)\\t\\0/
        @players << $1 unless @players.include?($1)
      end
    end
    refute_nil @players
  end

  def test_get_players_match_regex
    @file.each do |line|
      assert true, "Player find" if line.match /n\\(.*)\\t\\/
    end
  end

  def test_get_kills_regex
    @file.each do |line|
      assert_match /Kill/i, "Kill"
    end
  end

  def test_total_kills
    total_kills = 0

    @file.each do |line|
      total_kills += 1 if line.match /Kill:/i
    end
    assert_operator total_kills, :>=, 0
  end

  def test_kills_by_means
    @file.each do |line|
      if line.match @kill_regex
        if @kills_by_means.has_key?($6)
          @kills_by_means[$6] += 1
        else
          @kills_by_means[$6] = 1
        end
      end
    end
    refute_nil @kills_by_means
  end
end
