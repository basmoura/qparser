require 'minitest/autorun'
require './lib/qparser'

class QparserTest < Minitest::Test
  def test_parser_not_be_nil
    refute_nil Qparser.load, "Can not be nil"
  end
end
